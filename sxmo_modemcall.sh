#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2022 Sxmo Contributors

# Include common definitions
# shellcheck source=configs/default_hooks/sxmo_hook_icons.sh
. sxmo_hook_icons.sh
# shellcheck source=scripts/core/sxmo_common.sh
. sxmo_common.sh

# We use this directory to store states, so it must exist
mkdir -p "$XDG_RUNTIME_DIR/sxmo_calls"

set -e

vid_to_number() {
	mmcli -m any -o "$1" -K | \
		grep call.properties.number | \
		cut -d ':' -f2 | \
		tr -d ' '
}

log_event() {
	EVT_HANDLE="$1"
	EVT_VID="$2"
	NUM="$(vid_to_number "$EVT_VID")"
	TIME="$(date +%FT%H:%M:%S%z)"
	mkdir -p "$SXMO_LOGDIR"
	printf "%s\t%s\t%s\n" "$TIME" "$EVT_HANDLE" "$NUM" >> "$SXMO_LOGDIR/modemlog.tsv"
}

pickup() {
	CALLID="$1"
	DIRECTION="$(mmcli --voice-status -o "$CALLID" -K | grep call.properties.direction | cut -d: -f2 | tr -d ' ')"
	case "$DIRECTION" in
		outgoing)
		#	if ! sxmo_modemaudio.sh setup_audio; then
		#		sxmo_notify_user.sh --urgency=critical "We failed to setup call audio"
		#		return 1
		#	fi
			if ! mmcli -m any -o "$CALLID" --start; then
				sxmo_notify_user.sh --urgency=critical "We failed to start the call"
				return 1
			fi
			touch "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.initiatedcall"
			log_event "call_start" "$CALLID"
			;;
		incoming)
			sxmo_log "Invoking pickup hook"
			sxmo_hook_pickup.sh
			if ! sxmo_modemaudio.sh setup_audio; then
				sxmo_notify_user.sh --urgency=critical "We failed to setup call audio"
				return 1
			fi
			if ! mmcli -m any -o "$CALLID" --accept; then
				sxmo_notify_user.sh --urgency=critical "We failed to accept the call"
				return 1
			fi
			touch "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.pickedupcall"
			log_event "call_pickup" "$CALLID"
			;;
		*)
			sxmo_notify_user.sh --urgency=critical "Couldn't initialize call with callid <$CALLID>; unknown direction <$DIRECTION>"
			rm "$XDG_RUNTIME_DIR/sxmo_calls/"* 2>/dev/null || true
			rm -f "$XDG_RUNTIME_DIR"/sxmo.ring.pid 2>/dev/null
			rm -f "$SXMO_NOTIFDIR"/incomingcall* 2>/dev/null
			return 1
			;;
	esac
}

hangup() {
	CALLID="$1"
	if [ -f "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.pickedupcall" ]; then
		rm -f "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.pickedupcall"
		touch "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.hangedupcall"
		log_event "call_hangup" "$CALLID"
		sxmo_log "sxmo_modemcall: Invoking hangup hook"
		sxmo_hook_hangup.sh
		sxmo_jobs.sh stop incall_menu
	else
		touch "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.discardedcall"
		log_event "call_discard" "$CALLID"
		sxmo_log "sxmo_modemcall: Invoking discard hook"
		sxmo_hook_discard.sh
		sxmo_jobs.sh stop incall_menu
	fi
	if ! mmcli -m any -o "$CALLID" --hangup; then
		if list_active_calls | grep -q "/$CALLID "; then
			sxmo_notify_user.sh --urgency=critical "We failed to hangup the call"
			return 1
		fi
	fi
}

list_active_calls() {
	mmcli -m any --voice-list-calls | \
		awk '$1=$1' | \
		grep -v terminated | \
		grep -v "No calls were found" | while read -r line; do
			CALLID="$(printf "%s\n" "$line" | awk '$1=$1' | cut -d" " -f1 | xargs basename)"
			if [ -e "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.mutedring" ]; then
				continue
			fi
			printf "%s\n" "$line"
	done
}

incall_menu() {
	sxmo_jobs.sh start second_call sxmo_modemcall.sh check_second_call &
	CALLID="$(list_active_calls | head -n 1 | cut -d" " -f1 | xargs basename)"
	NUMBER="$(vid_to_number "$CALLID")"
	CONTACT="$(sxmo_contacts.sh --name-or-number "$NUMBER")"
	sxmo_keyboard.sh close &
	sxmo_dmenu.sh close &
	while list_active_calls | grep -E 'ringing-out|active' ; do
		CHOICES="$(
			grep . <<EOF
$icon_aru Volume ($(sxmo_audio.sh vol get)) ^ sxmo_audio.sh vol up
$icon_ard Volume ^ sxmo_audio.sh vol down
$icon_aru MicVol ($(pactl get-source-volume @DEFAULT_SOURCE@ | head -n1 | cut -d'/' -f2 | sed 's/ //g' | sed 's/%//')) ^ pactl set-source-volume @DEFAULT_SOURCE@ +"${1:-5}%"
$icon_ard MicVol ^ pactl set-source-volume @DEFAULT_SOURCE@ -"${1:-5}%"
$(sxmo_modemaudio.sh is_muted_mic && printf "%s Mic ^ sxmo_modemaudio.sh unmute_mic" "$icon_tof" || printf "%s Mic ^ sxmo_modemaudio.sh mute_mic" "$icon_ton")
$(
	list_active_calls | while read -r line; do
		CALLID="$(printf %s "$line" | cut -d" " -f1 | xargs basename)"
		NUMBER="$(vid_to_number "$CALLID")"
		CONTACT="$(sxmo_contacts.sh --name-or-number "$NUMBER")"
		case "$line" in
			*"(ringing-out)")
				printf "%s DTMF Tones %s ^ sxmo_terminal.sh sxmo_dtmf.sh %s\n" "$icon_mus" "$CONTACT" "$CALLID"
				printf "%s Hangup %s ^ hangup %s\n" "$icon_phx" "$CONTACT" "$CALLID"
				;;
			*"(active)")
				printf "%s DTMF Tones %s ^ sxmo_terminal.sh sxmo_dtmf.sh %s\n" "$icon_mus" "$CONTACT" "$CALLID"
				printf "%s Hangup %s ^ hangup %s\n" "$icon_phx" "$CONTACT" "$CALLID"
				;;
		esac
	done
)
EOF
	)"
		PICKED="$(
			printf "%s\n" "$CHOICES" |
			cut -d'^' -f1 |
			sxmo_dmenu.sh -i -H 50 -p "$icon_phn $CONTACT"
		)" || exit
		sxmo_log "Picked is $PICKED"
		CMD="$(printf "%s\n" "$CHOICES" | grep "$PICKED" | cut -d'^' -f2)"
		sxmo_log "Eval in call context: $CMD"
		eval "$CMD" || exit 1
	done &
	wait
}


mute() {
	CALLID="$1"
	touch "$XDG_RUNTIME_DIR/sxmo_calls/${CALLID}.mutedring"
	sxmo_log "Invoking mute_ring hook"
	sxmo_hook_mute_ring.sh
	log_event "ring_mute" "$1"
}

incoming_call_menu() {
	sxmo_dmenu.sh close &
	sxmo_keyboard.sh close &
	NUMBER="$(vid_to_number "$1")"
	NUMBER="$(sxmo_modem.sh cleanupnumber "$NUMBER")"
	CONTACTNAME="$(sxmo_contacts.sh --name-or-number "$NUMBER")"
	pickup_height="100"
	[ "$SXMO_WM" = "sway" ] && pickup_height="40"
	(
		PICKED="$(
			cat <<EOF | sxmo_dmenu.sh -i -H "$pickup_height" -p "$icon_phn $CONTACTNAME" -l 4
$icon_phn Pickup
$icon_phx Hangup
$icon_mut Ignore
$icon_msg I will call You later SMS
EOF
		)" || exit
		case "$PICKED" in
			"$icon_phn Pickup")
				if ! pickup "$1"; then
					sxmo_notify_user.sh --urgency=critical "We failed to pickup the call"
					sxmo_modemaudio.sh reset_audio
					return 1
				fi

				sxmo_jobs.sh start incall_menu sxmo_modemcall.sh incall_menu
				;;
			"$icon_phx Hangup")
				hangup "$1"
				;;
			"$icon_mut Ignore")
				mute "$1"
				;;
			"$icon_msg I will call You later SMS")
				hangup "$1"
				sxmo_modemsendsms.sh "$NUMBER" "Olen juuri nyt varattu, mutta soitan sinulle takaisin"
				;;
		esac
	) &
	wait
}

killed() {
	sxmo_dmenu.sh close
}

if [ "$1" = "incall_menu" ] || [ "$1" = "incoming_call_menu" ]; then
	trap 'killed' TERM INT
fi

check_second_call() {
	while true; do
		ACTIVE_CALLS=$(list_active_calls | grep "(waiting)" | wc -l)
		if [ "$ACTIVE_CALLS" -gt 0 ]; then
			CALLID="$(list_active_calls | grep "(waiting)" | head -n 1 | cut -d' ' -f1 | xargs basename)"
			NUMBER="$(vid_to_number "$CALLID")"
			CONTACTNAME="$(sxmo_contacts.sh --name-or-number "$NUMBER")"
			sxmo_vibrate 1000 1000 &
			notify-send "Incoming call from $CONTACTNAME"
			break
		fi
		sleep 5
	done
}

"$@"
